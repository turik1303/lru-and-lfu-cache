package cache.impl;

import cache.Cache;
import cache.CacheBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LFUCacheTest {

    private Cache cache;
    private static final int CAPACITY = 100;

    @BeforeEach
    void setUp() {
        this.cache = new CacheBuilder()
                .capacity(CAPACITY)
                .type(CacheBuilder.STRATEGY_LFU)
                .build();
    }

    @Test
    void put() throws InterruptedException {
        prefill();
        // PUTTING ITEM WITH EXISTING KEY
        // EMULATION OF CACHE HITS
        for (int i = 0; i < 10; i++) {
            cache.get(0L);
        }
        cache.put(CAPACITY / 2L, "TEST ITEM");
        Assertions.assertEquals(CAPACITY, cache.size(), "After insertion in full cache oldest item should be evicted and new one must be added");
        Assertions.assertTrue(cache.contains(CAPACITY / 2L), "Cache must contain item with key = CAPACITY / 2");
        // PUTTING ITEM WITH NOT EXISTING KEY
        cache.put("TEST ITEM KEY", "TEST ITEM 1");
        Assertions.assertTrue(cache.contains("TEST ITEM KEY"), "Cache must contain item with key = \"TEST ITEM KEY\"");
        Assertions.assertFalse(cache.contains("NOT EXISTING KEY"), "Cache must not contain item with key = \"NOT EXISTING KEY\"");
    }

    @Test
    void get() {
        Assertions.assertNull(cache.get(1), "Get method on empty cache must return null");
        cache.put(1, "First record");
        Assertions.assertEquals("First record", cache.get(1), "Item with key = 1 must contain string \"First record\"");
    }

    private void prefill() throws InterruptedException {
        for (long i = 0; i < CAPACITY; i++) {
            this.cache.put(i, Math.random());
            Thread.sleep(10);
        }
    }
}