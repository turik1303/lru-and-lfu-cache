package cache;

import cache.impl.LFUCache;
import cache.impl.LRUCache;

/**
 * Class to build cache
 */
public class CacheBuilder {

    public static final int STRATEGY_LRU = 0;
    public static final int STRATEGY_LFU = 1;

    private int capacity = 100;
    private int type = 0;

    public CacheBuilder capacity(int capacity) {
        this.capacity = capacity;
        return this;
    }

    public CacheBuilder type(int type) {
        this.type = type;
        return this;
    }

    public Cache build() {
        switch (this.type) {
            case 0: return new LRUCache(capacity);
            case 1: return new LFUCache(capacity);
            default: return null;
        }
    }
}
