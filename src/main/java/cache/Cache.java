package cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Base cache class
 */
public abstract class Cache {

    /**
     * maximum size of cache storage
     */
    private int maxCapacity;

    /**
     * storage container
     */
    protected LinkedHashMap<Object, CacheItem> storage;

    /**
     * Base cache constructor
     * @param maxCapacity maximum cache size
     */
    public Cache(int maxCapacity) {
        this.maxCapacity = maxCapacity;
        this.storage = new LinkedHashMap<>();
    }

    /**
     * Method saves item in cache
     * @param key key to extract stored item
     * @param data stored data
     */
    public abstract void put(Object key, Object data);

    /**
     * Method extracts data from cache by key
     * @param key key to extract stored item
     * @return stored data or null if no data found
     */
    public abstract Object get(Object key);

    /**
     * Method returns current size of storage
     * @return current size of storage
     */
    public int size() {
        return storage.size();
    }

    /**
     * Method checks if item with passed key exists in storage
     * @param key key of stored item
     * @return true if item exists, false if not exists
     */
    public boolean contains(Object key) {
        return storage.containsKey(key);
    }

    /**
     * Method removes all data from cache
     */
    public void clear() {
        storage.clear();
    }

    /**
     * Method checks if storage is full
     * @return true if storage is full, and false if not
     */
    protected boolean isStorageFull() {
        return storage.size() == this.maxCapacity;
    }

    /**
     * Method finds key of item to evict
     * @return key of most old or not popular item in cache
     */
    protected Object getKeyForEvict() {
        long minFactor = Long.MAX_VALUE;
        Object key = null;
        for (Map.Entry<Object, CacheItem> entry : storage.entrySet()) {
            if (minFactor > entry.getValue().getFactor()) {
                minFactor = entry.getValue().getFactor();
                key = entry.getKey();
            }
            if (minFactor == 0) {
                break;
            }
        }

        return key;
    }
}
