package cache.impl;

import cache.Cache;
import cache.CacheItem;

import java.util.Date;

public class LRUCache extends Cache {

    public LRUCache(int maxCapacity) {
        super(maxCapacity);
    }

    @Override
    public void put(Object key, Object data) {
        if (isStorageFull() && !storage.containsKey(key)) {
            storage.remove(getKeyForEvict());
        }
        CacheItem item = new CacheItem(new Date().getTime(), data);
        storage.put(key, item);
    }

    @Override
    public Object get(Object key) {
        CacheItem item = storage.get(key);
        if (item != null) {
            item.setFactor(new Date().getTime());
            return item.getData();
        }
        return null;
    }

}
