package cache.impl;

import cache.Cache;
import cache.CacheItem;

public class LFUCache extends Cache {

    public LFUCache(int maxCapacity) {
        super(maxCapacity);
    }

    @Override
    public void put(Object key, Object data) {
        long factor = 0L;
        if (isStorageFull() && !storage.containsKey(key)) {
            storage.remove(getKeyForEvict());
        }
        if (storage.containsKey(key)) {
            factor = storage.get(key).getFactor() + 1;
        }
        CacheItem item = new CacheItem(factor, data);
        storage.put(key, item);
    }

    @Override
    public Object get(Object key) {
        CacheItem item = storage.get(key);
        if (item != null) {
            long currentFactor = item.getFactor() + 1;
            item.setFactor(currentFactor);
            return item.getData();
        }
        return null;
    }

}
