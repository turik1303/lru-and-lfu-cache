package cache;

/**
 * Class for saving cached data
 */
public class CacheItem {
    /**
     * Counter to evict element
     */
    private long factor;
    /**
     * Cached data
     */
    private Object data;

    public CacheItem(long factor, Object data) {
        this.factor = factor;
        this.data = data;
    }

    public long getFactor() {
        return factor;
    }

    public void setFactor(long factor) {
        this.factor = factor;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
